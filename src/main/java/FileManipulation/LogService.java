package main.java.FileManipulation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LogService {
    File file;
    FileWriter writer;

    public LogService(){
        try {
            this.file = new File("src/main/resources/log.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Method for writing events to log
    public void write(String event, long exeTime){
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-mm-yyyy hh:mm");
        String time = LocalDateTime.now().format(format);
        try {
            writer = new FileWriter(file, true);
            writer.append(time + "\n" + event + "The function took " + exeTime + "ms to execute.\n\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

