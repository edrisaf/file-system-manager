package main.java.FileManipulation;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FileService {
    String uri = "src/main/resources/Files";
    File folder;
    File dracula;
    File[] listOfFiles;
    ArrayList<String> extensionsList;
    LogService log;

    // Constructor to initialize directory and dracula file
    // The File constructor does not throw an exception in case the file is not found, so I implemented it myself
    public FileService() {
        this.log = new LogService();
        try {
            this.folder = new File(uri);
            this.dracula = new File(uri + "/dracula.txt");
            if (!folder.exists() || !dracula.exists()) throw new FileNotFoundException("Cannot locate your files.");
        } catch (Exception e){
            System.out.println("Invalid pathname");
            e.printStackTrace();
        }
    }

    // Method to list all available files in the directory
    public void listAllFiles() {
        long start = System.currentTimeMillis();
        String msg = "Files in directory:\n";
        listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            msg += file.getName() + "\n";
        }
        long end = (System.currentTimeMillis() - start);
        log.write(msg, end);
        System.out.println(msg);
    }

    // Method to print all unique extensions in the directory using regex to split by "." and add to list if unique
    public void availableExtensions() {
        long start = System.currentTimeMillis();
        String msg = "Available extensions:\n";
        extensionsList = new ArrayList<>();
        for (File file : listOfFiles) {
            String fileExt = file.getName().split("\\.")[1];
            if (!extensionsList.contains(fileExt)) {
                extensionsList.add(fileExt);
                msg += extensionsList.indexOf(fileExt) + ". " + fileExt + "\n";
            }
        }
        long end = System.currentTimeMillis() - start;
        log.write(msg, end);
        System.out.println(msg);
    }

    // Method which takes the index of specific extension in the extensions list and searches for all files with that extension
    public void listFilesByExtension(int extIndex) {
        long start = System.currentTimeMillis();
        String ext = extensionsList.get(extIndex);
        String msg = "Searching for " + ext + " files:\n" ;
        for (File file : listOfFiles) {
            if (file.getName().endsWith(ext)) {
                msg += file.getName() + "\n";
            }
        }
        long end = System.currentTimeMillis() - start;
        log.write(msg, end);
        System.out.println(msg);
    }

    // Method to print information about the dracula file
    public void getDraculaInfo(){
        long start = System.currentTimeMillis();
        String msg = "Filename: " + dracula.getName() + "\nSize: " + dracula.length()/1024 +"kb" + "\nNumber of lines: " + getNLines();
        System.out.println(msg);
        long end = System.currentTimeMillis() - start;
        log.write(msg, end);
    }

    // Method to count number of lines in dracula. This method is called upon in getDraculaInfo()
    public int getNLines(){
        int counter = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(dracula))){
            while(reader.readLine() != null) {
                counter += 1;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return counter;
    }

    // Method to search for a specific word. This method also takes userChoice as input, which decides if it's a search
    // for existence or to count occurrences.
    public String searchWord(String word, int userChoice) {
        long start = System.currentTimeMillis();
        String msg = "";
        word = word.toLowerCase();
        int counter = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(dracula))){
            String line = reader.readLine();
            while(line != null) {
                if (line.toLowerCase().contains(word)){
                    List<String> words = Arrays.asList(line.toLowerCase().split("[\\p{Punct}\\s]+"));
                    counter += Collections.frequency(words, word);
                    if (counter > 0 && userChoice == 0){
                        msg += ("The word '" + word + "' exists!" + "\n");
                        log.write(msg, System.currentTimeMillis() - start);
                        return msg;
                    }
                }
                line = reader.readLine();

            }
            msg += "Occurrences of the word '" + word + "': " + counter + "\n";
            log.write(msg, System.currentTimeMillis()-start);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return msg;
    }
}