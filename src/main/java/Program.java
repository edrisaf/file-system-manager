package main.java;

import main.java.FileManipulation.FileService;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        FileService fileService = new FileService();
        Scanner scanner = new Scanner(System.in);

        boolean quit = false;
        while (!quit){
            System.out.println("Options:");
            showStartMenu();
            int choice = scanner.nextInt();
            switch (choice){
                case 1:
                    fileService.listAllFiles();
                    wait(1000);
                    break;
                case 2:
                    System.out.println("Choose an extension to search for");
                    fileService.availableExtensions();
                    fileService.listFilesByExtension(scanner.nextInt());
                    wait(1000);
                    break;
                case 3:
                    fileService.getDraculaInfo();
                    wait(1000);
                    break;
                case 4:
                    System.out.println("Enter the word you wish to search for:");
                    System.out.println(fileService.searchWord(scanner.next(), 0));
                    wait(1000);
                    break;
                case 5:
                    System.out.println("Enter the word you wish to search for:");
                    System.out.println(fileService.searchWord(scanner.next(), 1));
                    wait(1000);
                    break;
                case 6:
                    System.out.println("Session ended.");
                    quit = true;
                    break;
                default:
                    System.out.println("Choose one of the options provided.");
                    wait(1000);
            }
        }

    }

    public static void showStartMenu(){
        System.out.println("1. List all files in directory");
        System.out.println("2. List files by extension");
        System.out.println("3. Get information on Dracula.txt");
        System.out.println("4. Search if a word exists in Dracula.txt");
        System.out.println("5. Search how many times a specific word occurred in Dracula.txt");
        System.out.println("6. End session");
    }

    // Method for delaying the menu from popping up instantly
    public static void wait(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

